package services

import (
	"gitlab.com/userInMongo/domain"
	"gitlab.com/userInMongo/utils"
)

type userService struct{}

var (
	UserService userService
)

func (*userService) GetUser(userId uint64) (*domain.User, *utils.ApplicationError) {
	user, err := domain.UserDao.GetUser(userId)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (*userService) CreateUser(user *domain.User) *utils.ApplicationError {
	err := domain.UserDao.CreateUser(user)
	if err != nil {
		return err
	}
	return nil
}

func (*userService) DeleteUser(userId uint64) *utils.ApplicationError {
	err := domain.UserDao.DeleteUser(userId)
	if err != nil {
		return err
	}
	return nil
}

func (*userService) UpdateUser(userId uint64, fieldName, data string) *utils.ApplicationError {
	err := domain.UserDao.UpdateUser(userId, fieldName, data)
	if err != nil {
		return err
	}
	return nil
}
