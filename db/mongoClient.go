package db

import (
	"context"
	"gitlab.com/userInMongo/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
)

func GetClient() (*mongo.Client, *utils.ApplicationError) {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		return nil, &utils.ApplicationError{
			Message:    "cannot access to MongoDB",
			StatusCode: http.StatusInternalServerError,
			Code:       "db_error",
		}
	}
	return client, nil
}



func GetCollection(collectionName string) (*mongo.Collection, *utils.ApplicationError) {
	client, err := GetClient()
	if err != nil {
		return nil, err
	}
	collection := client.Database("userdb").Collection("users")
	return collection, nil
}
