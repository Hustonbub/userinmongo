package domain

import (
	"context"
	"fmt"
	"gitlab.com/userInMongo/db"
	"gitlab.com/userInMongo/utils"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

var (
	UserDao userDaoInterface
)

func init() {
	UserDao = &userDao{}
}

type userDaoInterface interface {
	GetUser(uint64) (*User, *utils.ApplicationError)
	CreateUser(*User) *utils.ApplicationError
	DeleteUser(uint64) *utils.ApplicationError
	UpdateUser(uint64, string, string) *utils.ApplicationError
}

type userDao struct{}

func (*userDao) GetUser(userId uint64) (*User, *utils.ApplicationError) {
	collection, err := db.GetCollection("users")
	if err != nil {
		return nil, err
	}
	document := collection.FindOne(context.TODO(), bson.M{"id": userId})
	var user User
	err2 := document.Decode(&user)
	if err2 != nil {
		return nil, &utils.ApplicationError{
			Message:    fmt.Sprintf("user %v cannot be found", userId),
			StatusCode: http.StatusNotFound,
			Code:       "not_found",
		}
	}
	return &user, nil
}

func (*userDao) CreateUser(user *User) *utils.ApplicationError {
	collection, err := db.GetCollection("users")
	if err != nil {
		return err
	}
	_, err2 := collection.InsertOne(context.TODO(), user)
	if err2 != nil {
		return &utils.ApplicationError{
			Message:    fmt.Sprintf("user %v cannot be created", user.ID),
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
	}
	return nil
}

func (*userDao) DeleteUser(userId uint64) *utils.ApplicationError {
	collection, err := db.GetCollection("users")
	if err != nil {
		return err
	}
	_, err2 := collection.DeleteOne(context.TODO(), bson.M{"id": userId})
	if err2 != nil {
		return &utils.ApplicationError{
			Message:    fmt.Sprintf("Cannot delete user %v", userId),
			StatusCode: http.StatusNotFound,
			Code:       "not_found",
		}
	}
	return nil
}

func (*userDao) UpdateUser(userId uint64, fieldName string, data string) *utils.ApplicationError {
	collection, err := db.GetCollection("users")
	if err != nil {
		return err
	}
	filter := bson.D{{"id", userId}}

	update := bson.D{
		{"$set", bson.D{
			{fieldName, data},
		}},
	}
	_, err2 := collection.UpdateOne(context.TODO(), filter, update)
	fmt.Println(err2)
	if err2 != nil {
		return &utils.ApplicationError{
			Message:    "Cannot update the user " + fieldName + " " + data,
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
	}
	return nil
}
